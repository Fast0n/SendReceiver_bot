import os
import sys
import telepot
from settings import token
from time import sleep

def on_chat_message(msg):
    content_type, chat_type, chat_id = telepot.glance(msg)

    '''
        #debug mode#

        response = bot.getUpdates()
        print (response)
    '''

    if content_type == 'text' and msg['text'] == '/start':
        bot.sendMessage(chat_id, "Ciao mondo")

    if content_type == 'text':
        # estai il testo inviato lo invia
        bot.sendMessage(chat_id, msg['text'])

    if content_type == 'document':
        # estai il nome del documento inviato
        name = msg['document']['file_name']
        # estrai l'id del documento inviato
        document = msg['document']['file_id']
        # scarica il documento inviato dal server
        bot.download_file(document, './' + name)
        # animazione di invio file
        bot.sendChatAction(chat_id, 'upload_document')
        # invio documento dalla cartella
        bot.sendDocument(chat_id, (name, open(name, 'rb')))

    if content_type == 'photo':
        try:
            # estrae il nome dell'immagine inviata
            name = msg['photo'][0]['file_path']
            name = name.replace("photos/", "")
            # estrae l'id dell'immagine inviata
            photo = msg['photo'][-1]['file_id']
        except KeyError:
            # estrae il nome dell'immagine inviata
            name = "file_tmp.jpg"          
            # estrae l'id dell'immagine inviata
            photo = msg['photo'][-1]['file_id']

        # scarica l'immagine inviata dal server
        bot.download_file(photo, './' + name)
        # animazione di dell'immagine file
        bot.sendChatAction(chat_id, 'upload_document')
        # invio dell'immagine dalla cartella
        bot.sendDocument(chat_id, (name, open(name, 'rb')))

    if content_type == 'audio':
        # prende il nome dall'id dell'audio
        name = msg['audio']['file_id']
        name = name + ".wav"
        # estrai l'id del l'audio inviato
        audio = msg['audio']['file_id']
        # scarica l'audio inviato dal server
        bot.download_file(audio, './' + name)
        # animazione di invio file
        bot.sendChatAction(chat_id, 'upload_document')
        # invio l'audio dalla cartella
        bot.sendDocument(chat_id, (name, open(name, 'rb')))

    if content_type == 'voice':
        # prende il nome dall'id dell'audio vocale
        name = msg['voice']['file_id']
        name = name + ".ogg"
        # estrae l'id del l'audio vocale inviato
        voice = msg['voice']['file_id']
        # scarica l'audio vocale inviato dal server
        bot.download_file(voice, './' + name)
        # animazione di invio file
        bot.sendChatAction(chat_id, 'upload_document')
        # invio l'audio dalla cartella
        bot.sendDocument(chat_id, (name, open(name, 'rb')))

# Main
print("Avvio SendReceiver_bot")

# PID file
pid = str(os.getpid())
pidfile = "/tmp/sendreceiver_bot.pid"

# Check if PID exist
if os.path.isfile(pidfile):
    print("%s already exists, exiting!" % pidfile)
    sys.exit()

# Create PID file
f = open(pidfile, 'w')
f.write(pid)

# Start working
try:
    bot = telepot.Bot(token)
    bot.message_loop(on_chat_message)
    while(1):
        sleep(10)
finally:
    os.unlink(pidfile)